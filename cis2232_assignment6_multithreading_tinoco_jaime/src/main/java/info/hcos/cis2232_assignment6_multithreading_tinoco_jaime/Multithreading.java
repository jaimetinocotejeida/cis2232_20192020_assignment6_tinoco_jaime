/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hcos.cis2232_assignment6_multithreading_tinoco_jaime;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jtinocotejeida
 */
public class Multithreading {

    private static String write = "";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Thread writer2 = new Thread(new Runnable() {
            @Override
            public void run() {
                String text = JOptionPane.showInputDialog("Enter a text");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Multithreading.class.getName()).log(Level.SEVERE, null, ex);
                }
                Multithreading.writeToFile(text);
                
                System.out.println("start1");
            }
        });

        Writer writer1 = new Writer();
        writer2.start();
        writer1.start();

    }

    public static void writeToFile(String textToWrite) {
        String FILE_NAME = "/cis2232/journal.txt";
        try {
//Create a file
            Files.createDirectories(Paths.get("/cis2232"));
        } catch (IOException ex) {
            System.out.println("io exception caught");
        }

//Also write to the file when a new camper was added!!
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            fw = new FileWriter(FILE_NAME, true);
            bw = new BufferedWriter(fw);
            bw.write(textToWrite);
            System.out.println("Done");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

}
