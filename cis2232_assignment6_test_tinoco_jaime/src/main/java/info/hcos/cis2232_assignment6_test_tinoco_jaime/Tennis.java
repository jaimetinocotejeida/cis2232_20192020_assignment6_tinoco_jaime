package info.hcos.cis2232_assignment6_test_tinoco_jaime;

import java.util.Scanner;

/**
 *
 * @author jtinocotejeida
 */
public class Tennis 
{
    // Variables
    private static int numberMem;
    private static boolean member;
    private static int lesson;
    private static double cost;
    // getters and setters
    public int getNumberMem() 
    {
        return numberMem;
    }
    public void setNumberMem(int numberMem) 
    {
        this.numberMem = numberMem;
    }
    public boolean isMember() 
    {
        return member;
    }
    public void setMember(boolean member) 
    {
        this.member = member;
    }

    public int getLesson() 
    {
        return lesson;
    }

    public void setLesson(int lesson) 
    {
        this.lesson = lesson;
    }
    public double getCost() 
    {
        return cost;
    }
    

    // Constants Member cost
    private static final int ONE_MEMBER_COST = 55;
    private static final int TWO_MEMBER_COST = 30;
    private static final int THREE_MEMBER_COST = 21;
    private static final int FOUR_MEMBER_COST = 16;
    // Constants Non Member cost
    private static final int ONE_NON_MEMBER_COST = 60;
    private static final int TWO_NON_MEMBER_COST = 33;
    private static final int THREE_NON_MEMBER_COST = 23;
    private static final int FOUR_NON_MEMBER_COST = 18;

    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to the CIS Tennis Program");
        System.out.println("How many are in the group(1,2,3,4)?: ");
        numberMem = input.nextInt();
        System.out.println("Are you a member(Y/N)?: ");
        String answer = input.next();
        if (answer.equalsIgnoreCase("Y")) 
        {
            member = true;
        } else 
        {
            member = false;
        }
        System.out.println("How many hours do you want for your lesson?: ");
        lesson = input.nextInt();
        calculateCost();
    }

    public static void calculateCost()
    {
        switch (numberMem) {
            case 1:
                if (member) {
                    cost = ONE_MEMBER_COST *lesson;
                } else {
                    cost = ONE_NON_MEMBER_COST *lesson ;
                }
                break;
            case 2:
                if (member) {
                    cost = TWO_MEMBER_COST *lesson;
                } else {
                   cost = TWO_NON_MEMBER_COST * lesson;
                }
                break;
            case 3:
                if (member) {
                    cost = THREE_MEMBER_COST * lesson;
                } else {
                    cost = THREE_NON_MEMBER_COST * lesson;
                }
                break;
            case 4:
                if (member) {
                    cost = FOUR_MEMBER_COST *lesson;
                } else {
                    cost =  FOUR_NON_MEMBER_COST *lesson;
                }
                break;
            default:
                break;
        }
        cost *= numberMem;
        System.out.println("The cost is $"+cost);    
    }
}
