/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import info.hcos.cis2232_assignment6_test_tinoco_jaime.Tennis;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author jtinocotejeida
 */
public class Tennis_test {
    
     Tennis tennis;
    public Tennis_test() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
     @Test
    public void costMembers4() {
        tennis = new Tennis();
        tennis.setNumberMem(3);
        tennis.setMember(true);
        tennis.setLesson(1);
        tennis.calculateCost();
        double result = tennis.getCost();
        assertEquals(64, result);
    }

    @Test
    public void costMembers2() {
        tennis = new Tennis();
        tennis.setNumberMem(2);
        tennis.setMember(true);
        tennis.setLesson(1);
        tennis.calculateCost();
        double result = tennis.getCost();
        assertEquals(60, result);
    }

    @Test
    public void costNonMembers2() {
        tennis = new Tennis();
        tennis.setNumberMem(2);
        tennis.setMember(false);
        tennis.setLesson(1);
        tennis.calculateCost();
        double result = tennis.getCost();
        assertEquals(66, result);
    }
}
